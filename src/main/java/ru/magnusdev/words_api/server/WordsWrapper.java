package ru.magnusdev.words_api.server;

import java.util.Collection;

public class WordsWrapper {

	private String originalWord;

	private Collection<String> words;

	public String getOriginalWord() {
		return originalWord;
	}

	public void setOriginalWord(String originalWord) {
		this.originalWord = originalWord;
	}

	public Collection<String> getWords() {
		return words;
	}

	public void setWords(Collection<String> words) {
		this.words = words;
	}

}
