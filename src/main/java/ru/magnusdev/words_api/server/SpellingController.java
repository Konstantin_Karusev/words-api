package ru.magnusdev.words_api.server;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.magnusdev.words_api.core.SpellingEngine;

@Path("spelling")
public class SpellingController {

	private static final Logger logger = LoggerFactory.getLogger(SpellingController.class);

	private static SpellingEngine spellingEngine = new SpellingEngine();

	@GET
	@Path("correct")
	@Produces(MediaType.APPLICATION_JSON)
	public WordsWrapper correct(@QueryParam("word") String word, @QueryParam("limit") int limit) {
		WordsWrapper result = new WordsWrapper();
		Collection<String> synonyms = spellingEngine.correct(word, limit);
		result.setWords(synonyms);
		return result;
	}

}
