package ru.magnusdev.words_api.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.magnusdev.words_api.core.Word2VecEngine;

@Path("word2vec")
public class Word2VecController {

	private static final Logger logger = LoggerFactory.getLogger(Word2VecController.class);

	private static Word2VecEngine w2vEngine = new Word2VecEngine();

	@GET
	@Path("synonymizeWord")
	@Produces(MediaType.APPLICATION_JSON)
	public WordsWrapper synonymizeWord(@QueryParam("word") String word, @QueryParam("limit") int limit) {
		WordsWrapper result = new WordsWrapper();
		Collection<String> synonyms = w2vEngine.synonymize(word, limit);
		result.setOriginalWord(word);
		result.setWords(synonyms);
		return result;
	}

	@GET
	@Path("synonymizeWords")
	@Produces(MediaType.APPLICATION_JSON)
	public List<WordsWrapper> synonymizeWords(@QueryParam("words") List<String> words, @QueryParam("limit") int limit) {
		List<WordsWrapper> result = new ArrayList<>();
		for (String word : words) {
			result.add(synonymizeWord(word, limit));
		}
		return result;
	}

	@GET
	@Path("similarity")
	@Produces(MediaType.TEXT_PLAIN)
	public Double similarity(@QueryParam("word") String word1, @QueryParam("word") String word2) {
		return w2vEngine.checkSimilarity(word1, word2);
	}

}
