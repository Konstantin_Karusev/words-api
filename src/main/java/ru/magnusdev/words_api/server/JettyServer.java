package ru.magnusdev.words_api.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JettyServer {

	private static final Logger logger = LoggerFactory.getLogger(JettyServer.class);

	public static void main(String[] args) {

		ResourceConfig rc = new ResourceConfig();
		Word2VecController w2vResource = new Word2VecController();
		SpellingController spellingResource = new SpellingController();
		rc.register(w2vResource);
		rc.register(spellingResource);

		ServletContainer sc = new ServletContainer(rc);
		ServletHolder servletHolder = new ServletHolder(sc);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.addServlet(servletHolder, "/*");

		Server jettyServer = new Server(8080);

		jettyServer.setHandler(context);

		try {
			jettyServer.start();
			jettyServer.join();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			jettyServer.destroy();
		}
	}

}
