package ru.magnusdev.words_api.core;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Context {

	private volatile static Context context;

	private static final Logger logger = LoggerFactory.getLogger(Context.class);
	private static Object monitor = new Object();

	public static Context getInstance() {
		if (context == null) {
			synchronized (monitor) {
				if (context == null) {
					context = new Context();
				}
			}
		}
		return context;
	}

	private Properties props;

	public Context() {
		try (InputStreamReader input = new InputStreamReader(new FileInputStream("config/params.properties"), "UTF-8");) {
			props = new Properties();
			props.load(input);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public String getProperty(String key) {
		return props.getProperty(key, "");
	}

}
