package ru.magnusdev.words_api.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpellingEngine {

	private static final Logger logger = LoggerFactory.getLogger(SpellingEngine.class);

	private final HashMap<String, Double> nWords = new HashMap<String, Double>();

	public SpellingEngine() {
		try (BufferedReader in = new BufferedReader(new FileReader(Context.getInstance().getProperty("path.model.spelling")));) {
			String line = "";
			while ((line = in.readLine()) != null) {
				String[] parts = line.split(" ");
				nWords.put(parts[2], Double.parseDouble(parts[1]));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	private final ArrayList<String> edits(String word) {
		ArrayList<String> result = new ArrayList<>();
		for (int i = 0; i < word.length(); ++i) {
			result.add(word.substring(0, i) + word.substring(i + 1));
		}
		for (int i = 0; i < word.length() - 1; ++i) {
			result.add(word.substring(0, i) + word.substring(i + 1, i + 2) + word.substring(i, i + 1) + word.substring(i + 2));
		}
		for (int i = 0; i < word.length(); ++i) {
			for (char c = 'а'; c <= 'я'; ++c) {
				result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i + 1));
			}
		}
		for (int i = 0; i <= word.length(); ++i) {
			for (char c = 'а'; c <= 'я'; ++c) {
				result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i));
			}
		}
		return result;
	}

	public final Collection<String> correct(String word, int limit) {
		if (nWords.containsKey(word)) {
			return new HashSet<>(Arrays.asList(word));
		}
		ArrayList<String> list = edits(word);
		TreeMap<Double, String> candidates = new TreeMap<>(Collections.reverseOrder());
		for (String s : list) {
			if (nWords.containsKey(s)) {
				candidates.put(nWords.get(s), s);
			}
		}

		if (candidates.size() > 0) {
			return subSet(candidates.values(), limit);
		}
		for (String s : list) {
			for (String w : edits(s)) {
				if (nWords.containsKey(w)) {
					candidates.put(nWords.get(w), w);
				}
			}
		}
		return candidates.size() > 0 ? subSet(candidates.values(), limit) : new HashSet<>(Arrays.asList(word));
	}

	private Collection<String> subSet(Collection<String> candidates, int limit) {
		Collection<String> result = new TreeSet<>();
		Iterator<String> iter = candidates.iterator();
		int actualSize = candidates.size() > limit ? limit : candidates.size();
		int i = 0;
		while (i++ < actualSize) {
			result.add(iter.next());
		}
		return result;
	}

}
