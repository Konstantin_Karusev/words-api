package ru.magnusdev.words_api.core;

import java.io.File;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

public class Word2VecModelCreator {

	public static void main(String[] args) {
		SentenceIterator iter = new LineSentenceIterator(new File(args[0]));
		iter.setPreProcessor(sentence -> sentence.toLowerCase());
		TokenizerFactory t = new DefaultTokenizerFactory();
		t.setTokenPreProcessor(new CommonPreprocessor());
		Word2Vec vec = new Word2Vec.Builder()
				.batchSize(1000)
				.workers(10)
				.sampling(1e-4)
				.minWordFrequency(5)
				.iterations(1)
				.layerSize(300)
				.seed(42)
				.windowSize(10)
				.iterate(iter).tokenizerFactory(t).build();
		vec.fit();
		WordVectorSerializer.writeFullModel(vec, args[1]);
	}

}
