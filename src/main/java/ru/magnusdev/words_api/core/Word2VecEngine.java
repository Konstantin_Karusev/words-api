package ru.magnusdev.words_api.core;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Word2VecEngine {

	private static final Logger logger = LoggerFactory.getLogger(Word2VecEngine.class);

	private Word2Vec vec;

	public Word2VecEngine() {
		try {
			logger.info("Initializing Word2Vec engine");
			logger.info("Reading model source file");
			File gModel = new File(Context.getInstance().getProperty("path.model.word2vec"));
			String modelType = Context.getInstance().getProperty("type.model.word2vec");
			logger.info("Loading model with type " + modelType);
			switch (modelType) {
			case "google":
				vec = (Word2Vec) WordVectorSerializer.loadGoogleModel(gModel, true);
				break;
			case "dl4j":
				vec = WordVectorSerializer.readWord2Vec(gModel);
				break;
			default:
				logger.error("Unknown model type");
			}
			logger.info("Word2Vec engine initialized");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Collection<String> synonymize(String word, int limit) {
		Collection<String> result = new HashSet<>();
		if (word != null && !word.isEmpty()) {
			result.addAll(vec.wordsNearest(word, limit));
		}
		return result;
	}

	public double checkSimilarity(String word1, String word2) {
		return vec.similarity(word1, word2);
	}

}
